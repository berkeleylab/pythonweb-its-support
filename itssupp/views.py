from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404,render
from django.template import loader
from django.urls import reverse
from .models import Choice, Question, Testing
from itssupp.Support_abandonFDorSPandChildren import Support_abandonFDorSPandChildren


def index(request):
    #template = loader.get_template('itssupp/index.html')
    return render(request, 'itssupp/index.html')



def abandonPage(request):
  return render(request, 'itssupp/abandonPage.html')


# this gets all the data from the input form and sends to abandonSPreport.html
def info(request):
    mySupportTask = Support_abandonFDorSPandChildren()
    form = request.GET   # get the input from the form on the page
    servername = form['server']
    ticketnum = form['ticketnum']
    function = form['function']
    spOrFd = form['SpsOrFds']

    mySupportTask.doAbandon(servername, ticketnum, function, spOrFd)
    NumErrors = str(mySupportTask.errorCnt)
    taskServer = mySupportTask.server
    taskDBcomment = mySupportTask.sComment
    taskFunction = mySupportTask.functionText
    sampleSet = mySupportTask.sampleSet
    sowSet = mySupportTask.sowSet
    spSet = mySupportTask.spSet
    confirmQuery = mySupportTask.confirmQuery
    workflowURL = mySupportTask.route2workflowURL
    taskServerName = mySupportTask.servername

    if (function == "SPchildren" or function == "SPchildrenTest"):
        return render(request, 'itssupp/abandonSPreport.html', {'server': taskServer,
                                                         'comment':taskDBcomment,
                                                         'function':taskFunction,
                                                         'samples':sampleSet,
                                                         'sows':sowSet,
                                                         'sps':spSet,
                                                         'query':confirmQuery,
                                                         'url': workflowURL,
                                                         'jiraTicket':ticketnum,
                                                         'serverName': taskServerName})
    else :   # (function == "FDchildren" or function == "FDchildrenTest"):
        apSet = mySupportTask.apSet
        atSet = mySupportTask.atSet
        fdSet = mySupportTask.fdSet
        return render(request, 'itssupp/abandonFDreport.html', {'server': taskServer,
                                                         'comment':taskDBcomment,
                                                         'function':taskFunction,
                                                         'samples':sampleSet,
                                                         'sows':sowSet,
                                                         'sps':spSet,
                                                         'aps': apSet,
                                                         'ats': atSet,
                                                         'fds': fdSet,
                                                         'query':confirmQuery,
                                                         'url': workflowURL,
                                                         'jiraTicket':ticketnum,
                                                         'serverName': taskServerName})




























