#from wsTools import wsTools
#from util_udfTools import udfTools

from itssupp.tools.util_dbTools import dbTools

#from util_jsonInput import jsonInput

#gets the samples, sows, sps, aps, ats  and FDs that are related to FD and abandons them. Also abandon FD
#user inputs on command line:
#         server  = prod or dev
#         itssupp jira ticket number
#         the fds or sps that need to be abandon and children. if sps are given, the associated fds will also be abandoned
# will output, all decendents of FD.  The update commands to abandon these entities, the query to confirm status update,
# and the URL and sample list to use in routetoworkflow to remove samples from clarity  queues

class Support_abandonFDorSPandChildren():
    def __init__(self):
        self.myDB = dbTools()
        self.errorCnt = 0
        self.sComment = ""
        self.server = ''
        self.route2workflowURL = "http://clarity-dev01.jgi-psf.org:8180/route-to-workflow"
        self.functionText = ""
        self.confirmQuery = ""

    #-----------------------------------------------------
    # get all childrend of related FDs  and abandon
    # inputs:  server -  "prod"  from production (pluss-genprd1), 'dev' for development (plus-dwint1)
    def doAbandon(self,server,ticketNum,function,spFDs):
        if server != 'Prod' and server != 'Dev' and server != 'Int':
            self.errorCnt = 1
        else:
            self.server = server
            #set the url for routetoworkflow
            if self.server == 'Prod':
                self.route2workflowURL = "http://clarity-prd01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-prd01"
            elif self.server == 'Dev':
                self.route2workflowURL = "http://clarity-dev01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-dev01"
            elif self.server == 'Int':
                self.route2workflowURL = "http://clarity-int01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-int01"
            else:
                self.route2workflowURL = ""
                self.servername = ""
                self.errorCnt = 1
                exit()

            self.myDB.connect(self.server)
            # set status comment for db update
            self.sComment = 'ITSSUPP-' + ticketNum + ' auto-updated by Support_abandonFDorSPandChildren.py'
            self.spsOrFds = spFDs   # save the sp ids (or fd ids ) from the input
            print (function)
            if function=="FDchildren":
                self.functionText = "abandonFDandChildren"
                self.abandonFDandChildren()
            elif function =="SPchildren":
                self.functionText = "abandonSPandChildren"
                self.abandonSPandChildren()
            elif function == "FDchildrenTest":
                print ("at FD Test")
                self.functionText = "abandonFDandChildrenTest"
                self.abandonFDandChildren()
            elif function == "SPchildrenTest":
                self.functionText = "abandonSPandChildrenTest"
                self.abandonSPandChildren()

            else:
                self.errorCnt = 1



    # ----------------------------------
    # abandon FD and Children
    # FD, APs, ATs, SPs, Samples, sows
    # ---------------------------------
    def abandonFDandChildren(self):
        fdId = self.getFDsToProcess()

        # get all samples associated with FD and abandon
        query = 'select sam.SAMPLE_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id where fd.final_deliv_project_id in (' + str(
            fdId) + ')'
        sampleList = self.myDB.doQueryGetAllRows(query)
        sampleSet = set(sampleList)
        self.sampleSet = sampleSet

        # get all sows associated with FD and abandon
        query = 'select sow.SOW_ITEM_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        self.sowSet = sowSet

        # get all sps associated with FD and abandon
        query = 'select sp.sequencing_project_id from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        spList = self.myDB.doQueryGetAllRows(query)
        spSet = set(spList)
        self.spSet = spSet

        # get all aps associated with FD and abandon
        query = 'select ap.ANALYSIS_PROJECT_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'
        apList = self.myDB.doQueryGetAllRows(query)
        apSet = set (apList)
        self.apSet = apSet

        # get all ats associated with FD and abandon
        query = 'select at.ANALYSIS_TASK_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id ' \
                'left join uss.DT_ANALYSIS_TASK at on  ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'

        atList = self.myDB.doQueryGetAllRows(query)
        atSet = set(atList)
        self.atSet = atSet


        # get all fds associated with FD and abandon
        query = 'select fd.final_deliv_project_id from uss.dt_final_deliv_project fd ' \
                'where fd.final_deliv_project_id in (' + str(fdId) + ')'
        fdList = self.myDB.doQueryGetAllRows(query)
        fdSet = set(fdList)
        self.fdSet = fdSet

        if self.functionText == "abandonFDandChildren":  # update database for  non-test function
            self.abandonSamples(sampleSet)
            self.abandonSows(sowSet)
            self.abandonSPs(spSet)
            self.abandonAPs(apSet)
            self.cancelATs(atSet)
            self.abandonFDs(fdSet)

        query = ("select distinct "
                 "fd.final_deliv_project_id as fdid, "
                 "fdcv.status as fd_status, "
                 "ap.ANALYSIS_PROJECT_ID as ap_id, "
                 "apcv.status as ap_status, "
                 "at.ANALYSIS_TASK_ID as at_id, "
                 "atcv.status as at_status, "
                 "sp.sequencing_project_id as spid, "
                 "spcv.STATUS as spstatus, "
                 "sp.SEQUENCING_PROJECT_NAME, "
                 "sam.SAMPLE_ID as samid, "
                 "samcv.STATUS as samstatus,"
                 "sow.SOW_ITEM_ID as sowid,"
                 "sowcv.STATUS as sowstatus "
                 "from uss.dt_final_deliv_project fd "
                 "left join uss.DT_ANALYSIS_PROJECT ap on fd.final_deliv_project_id = ap.final_deliv_project_id "
                 "left join uss.DT_ANALYSIS_TASK at on  ap.ANALYSIS_PROJECT_ID = at.ANALYSIS_PROJECT_ID "
                 "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id "
                 "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                 "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                 "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                 "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.dt_sample_status_cv samcv ON samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_FINAL_DELIV_PROJ_STATUS_CV fdcv ON fdcv.STATUS_ID = fd.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_ANALYSIS_TASK_STATUS_CV atcv ON atcv.STATUS_ID = at.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_ANALYSIS_PROJECT_STATUS_CV apcv ON apcv.STATUS_ID = ap.CURRENT_STATUS_ID "
                 "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdId + ");" )

        self.confirmQuery = query




    #-----------------------------------------------------
    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command
    #
    def getFDsToProcess(self):
        fdORsp = self.spsOrFds

        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        fdCount = self.myDB.doQuery(fdCountquery)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
                self.errorCnt += 1
        else:
            fdId = fdORsp
        return fdId


    # -----------------------------------------------------
    # abandon SP and children
    # SPs, samples and sows only
    # ------------------------------------------------------
    def abandonSPandChildren(self):
        spId = self.getSPsToProcess()

        # get all samples from DB
        query = 'select sam.SAMPLE_ID from uss.dt_sequencing_project sp ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id ' \
                'left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id ' \
                'where sp.sequencing_project_id in (' + str(spId) + ')'
        # print(query)
        sampleList = self.myDB.doQueryGetAllRows(query)
        sampleSet = set(sampleList)
        self.sampleSet = sampleSet

        # get all sows
        query = 'select sow.SOW_ITEM_ID from uss.dt_sow_item sow ' \
                'where sow.sequencing_project_id in (' + str(spId) + ')'
        # print (query)
        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        self.sowSet = sowSet

        # get all sps
        query = 'select sp.sequencing_project_id from uss.dt_sequencing_project sp ' \
                'where sp.sequencing_project_id in (' + str(spId) + ')'
        spList = self.myDB.doQueryGetAllRows(query)
        spSet = set(spList)
        self.spSet = spSet

        if self.functionText == "abandonSPandChildren":   # update database for  non-test function
            self.abandonSamples(sampleSet)
            self.abandonSows(sowSet)
            self.abandonSPs(spSet)

        query = ("select distinct "
                 "sp.sequencing_project_id as spid, "
                 "spcv.STATUS as spstatus, "
                 "sp.SEQUENCING_PROJECT_NAME, "
                 "sam.SAMPLE_ID as samid, "
                 "samcv.STATUS as samstatus,"
                 "sow.SOW_ITEM_ID as sowid,"
                 "sowcv.STATUS as sowstatus "
                 "from uss.dt_sequencing_project sp "
                 "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                 "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                 "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                 "LEFT JOIN uss.dt_sow_item_status_cv sowcv ON sowcv.STATUS_ID = sow.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.dt_sample_status_cv samcv ON samcv.STATUS_ID = sam.CURRENT_STATUS_ID "
                 "LEFT JOIN uss.DT_SEQ_PROJECT_STATUS_CV spcv ON spcv.STATUS_ID = sp.CURRENT_STATUS_ID "
                 "where  sp.sequencing_project_id in (" + spId + ");")
        self.confirmQuery = query


    # -----------------------------------------------------
    # get the input from user,
    # separated by comma.  Will return the list of SPs associated with command
    #

    def getSPsToProcess(self):
        spList = self.spsOrFds
        spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + spList + ")"
        spCount = self.myDB.doQuery(spCountquery)
        if int(spCount) > 0:  # sps were inputted
            spId = ",".join(map(str, set(spList)))
        else:
            print("***** no SPs found in DB ******")
            self.errorCnt += 1
            spID = []

        return spList


    #---------------------------------------------------------------------------------
    def abandonSamples(self, sampleSet):

        # abandon the samples
        print("-------------------------------------------")
        print("abandon samples:", sampleSet, "if not abandoned or deleted already")
        if 'None' in sampleSet :
            print("*** no samples to abandon")

        else:
            y = ",".join(map(str, sampleSet))
            query = "update uss.dt_sample set current_status_id=13, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where sample_id in (" + y + ") " \
                                                                                         "and current_status_id not in (13,16)"
            print(query)
            self.myDB.doUpate(query)


    # ---------------------------------------------------------------------------------
    def abandonSows(self,sowSet):

        # abandon the sows
        print("-------------------------------------------")
        print("abandon sows:", sowSet, "if not abandoned, completed or deleted already")
        if 'None' in sowSet :
            print("*** no sows to abandon")

        else:
            y = ",".join(map(str, sowSet))
            query = "update uss.dt_sow_item set current_status_id=7, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where sow_item_id in (" + y + ") " \
                                                                                           "and current_status_id not in (6,7,10)"
            print(query)
            self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    def abandonSPs(self,spSet):

        # abandon the sps
        print("-------------------------------------------")
        print("abandon SPs:", spSet, "if not abandoned, completed or deleted already")
        if 'None' in spSet:
            print("*** no SPs to abandon")

        else:
            y = ",".join(map(str, spSet))
            query = "update uss.dt_sequencing_project  set current_status_id=7, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where sequencing_project_id in (" + y + ") " \
                                                                                                     "and current_status_id not in (6,7,9)"
            print(query)
            self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # abandon the aps
    def abandonAPs(self,apSet):
        print("-------------------------------------------")
        print("abandon APs:", apSet, "if not abandoned,completed or deleted already")
        if 'None' in apSet:
            print("*** no APs to abandon")

        else:
            y = ",".join(map(str, apSet))
            query = "update uss.DT_ANALYSIS_Project  set current_status_id=3, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where ANALYSIS_PROJECT_ID in (" + y + ") " \
                                                                                                   "and current_status_id not in (2,3,4)"
            print(query)
            self.myDB.doUpate(query)

    # ---------------------------------------------------------------------------------
    # cancel the ats
    def cancelATs(self,atSet):

        print("-------------------------------------------")
        print("cancel ats:", atSet, "if not cancelled,done or deleted already")
        if 'None' in atSet:
            print("*** no ATs to cancel")

        else:
            y = ",".join(map(str, atSet))
            query = "update uss.DT_ANALYSIS_TASK  set current_status_id=10, " \
                    "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                            "where ANALYSIS_TASK_ID in (" + y + ") " \
                                                                                                "and current_status_id not in (10,13,9)"
            print(query)
            self.myDB.doUpate(query)

    def abandonFDs(self,fdSet):
        # ---------------------------------------------------------------------------------
        # abandon the FDS
        print("-------------------------------------------")
        print("abandon fds:", fdSet, "if not abandoned,completed or deleted already")
        y = ",".join(map(str, fdSet))
        query = "update uss.dt_final_deliv_project  set current_status_id=6, " \
                "status_comments = '" + self.sComment + "',status_date = sysdate " \
                                                        "where FINAL_DELIV_PROJECT_ID in (" + y + ") " \
                                                                                                  "and current_status_id not in (5,6,8)"
        print(query)
        self.myDB.doUpate(query)







