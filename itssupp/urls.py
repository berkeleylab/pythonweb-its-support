from django.urls import path

from . import views
app_name = 'itssupp'


urlpatterns = [
    path('', views.index, name='index'),
    path('abandonPage', views.abandonPage, name='abandonPage'),
    path('info/', views.info),

]